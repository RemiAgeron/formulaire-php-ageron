<!DOCTYPE html>
<html>
<head>
	<title>Exo PHP</title>
</head>
<body>

	<form action="index.php" method="post">
		<label for="age">Quel age avez-vous ? </label>
		<input id="age" type="number" name="age">
		<p>Quel votre genre ? </p>
		<input id="homme" type="radio" name="genre" value="Homme"><label for="homme">Homme</label>
		<input id="femme" type="radio" name="genre" value="Femme"><label for="femme">Femme</label>
		<div>
			<input type="submit" name="submit" value="Calculez le prix de mon assurance">
		</div>
	</form>

	<?php
		if(empty($_POST) == false) {
		// Ici, on sait que l'utilisateur à envoyé le formulaire.

		// 1. Vérifier que l'utilisateur à bien donner son age.
		// Sinon on lui affiche un message d'erreur en rouge

		// 2. Vérifier que l'utilisateur à bien donner son genre.
		// Sinon on lui affiche un message d'erreur en rouge

		// 3. Traiter la demande
		
		// Si l'utilisateur a moins de 18 ans, on lui affiche un message en orange : "Trop jeune désolé"

		// Si l'utilisateur est un homme:
		// Si l'utilisateur a plus de 60 ans, il doit payer 50€ 
		// Si l'utilisateur a moins ou 60 ans, il doit payer 40€ 

		// Si l'utilisateur est un femme:
		// Si l'utilisateur a plus de 60 ans, il doit payer 45€ 
		// Si l'utilisateur a moins ou 60 ans, il doit payer 35€ 

		// @moncode
			
			if($_POST["age"] != null) {

				if($_POST["genre"] != null) {

					switch ($_POST["genre"]) {

						case Homme:
							switch ($_POST["age"]) {
								case ($_POST["age"] < 18):
									echo '<span class="orangewarning">Trop jeune désolé</span>';
									break;
								case ($_POST["age"] < 60):
									echo "Vous devez payer 40€";
									break;
								default:
									echo "Vous devez payer 50€";
							};
							break;
						
						case Femme:
							switch ($_POST["age"]) {
								case ($_POST["age"] < 18):
									echo '<span class="orangewarning">Trop jeune désolé</span>';
									break;
								case ($_POST["age"] < 60):
									echo "Vous devez payer 35€";
									break;
								default:
									echo "Vous devez payer 45€";
							};
							break;
	
					}

				} else {
					echo '<span class="redwarning">Il faut Choisir son genre aussi !</span>';
				}

			} else {
				
				if ($_POST["genre"] == null) {
					echo '<span class="redwarning">Il faut donner son âge et son genre !</span>';
				} else {
					echo '<span class="redwarning">Il faut donner son âge !</span>';
				}
			}

		}
	?>

	<style>
		body{ font-family: 'Liberation Sans'; margin: 20px; }
		label{ margin-right: 10px }
		div{ margin: 15px 0; }
		.orangewarning{ color: #FFA500; }
		.redwarning{ color: #FF0000; }
	</style>
	
</body>
</html>